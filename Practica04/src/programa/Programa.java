package programa;

import clases.Menus;
import clases.Polvorin;
/**
 * 
 * @author ManuelGuiuRodriguez
 *
 */
public class Programa {
	
	public static void main(String[] args) {
		
		Polvorin polvorin1 = new Polvorin(7);

		System.out.println("Binenvenido a el registro de polvorines:");
		Menus.menuPrincipal(polvorin1);
		
	}

}