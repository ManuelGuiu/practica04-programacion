package clases;
/**
 * 
 * @author ManuelGuiuRodriguez
 *
 */
public class Municion {
	
	/**
	 * @param String codigoMunicion, hace referencia al codigo de identificaion
	 * 
	 * @param String tipoMunicion, indica tipo de municion
	 * 
	 * @param String calibre, indica el calibre
	 * 
	 * @param String fabricante, indica el fabricante
	 * 
	 * @parame double alcanceEfectivo, indica el alcance efectivo
	 */

	private String codigoMunicion;
	private String tipoMunicion;
	private String calibre;
	private String fabricante;
	private double alcanceEfectivo;

	/**
	 * constructor con codigo de municion
	 * 
	 * @param String codigoMunicion
	 */
	public Municion(String codigoMunicion) {
		this.codigoMunicion = codigoMunicion;
	}

	public String getCodigoMunicion() {
		return codigoMunicion;
	}

	public void setCodigoMunicion(String codigoMunicion) {
		this.codigoMunicion = codigoMunicion;
	}

	public String getTipoMunicion() {
		return tipoMunicion;
	}

	public void setTipoMunicion(String tipoMunicion) {
		this.tipoMunicion = tipoMunicion;
	}

	public String getCalibre() {
		return calibre;
	}

	public void setCalibre(String calibre) {
		this.calibre = calibre;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public double getAlcanceEfectivo() {
		return alcanceEfectivo;
	}

	public void setAlcanceEfectivo(double alcanceEfectivo) {
		this.alcanceEfectivo = alcanceEfectivo;
	}
	
	/**
	 * @override
	 * */
	public String toString() {
		return codigoMunicion + ", " + tipoMunicion + ", " + calibre + ", " + fabricante + ", " + alcanceEfectivo;
	}
}
