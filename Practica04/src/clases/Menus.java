package clases;

import java.util.Scanner;

/**
 * 
 * @author ManuelGuiuRodriguez
 *
 */
public class Menus {

	static private String codigoMunicion, tipoMunicion, calibre, fabricante, alcanceF;
	static private double alcanceEfectivo;
	static private boolean continuar = false;
	static private Scanner input = new Scanner(System.in);
	static private String menu;

	/**
	 * Comprueba dentro del paremetro recibo si el String que se pide existe en el
	 * parametro
	 * 
	 * @param polvorin1
	 * @return cadena
	 */
	public static String comprobarCodigoMunicion(Polvorin polvorin1) {
		String cadena;
		do {
			System.out.println("Introduce el codigo de la municion que deseas cambiar.");
			cadena = input.nextLine();

			if (polvorin1.buscarMunicion(cadena) == null) {
				System.out.println("El codigo introducido no coincide con ninguna municion en los registros,");
				System.out.println("introduce un codigo valido.");
				continuar = true;
			} else {
				continuar = false;
			}
		} while (continuar);
		return cadena;
	}

	/**
	 * Metodo recibe parametro objeto muestra menu y realiza acciones, cambio de
	 * datos de objeto
	 * 
	 * @param polvorin1
	 * @return
	 */
	public static Polvorin menuCambiar(Polvorin polvorin1) {
		System.out.println(
				"De la siguiente lista introduce el numero correspondiente al parametro que deseas modificar.");
		System.out.println("1. Cambiar el codigo de municion.");
		System.out.println("2. Cambiar el tipo de municion.");
		System.out.println("3. Cambiar el calibre de la municion.");
		System.out.println("4. Cambiar el fabricante.");
		System.out.println("5. Cambiar el alcance efectivo de la municion.");
		menu = input.nextLine();
		switch (menu) {
		case "1":
			codigoMunicion = comprobarCodigoMunicion(polvorin1);

			System.out.println("Introduce el nuevo valor que deseas introducir.");
			String nuevoCodigoMunicion = input.nextLine();
			polvorin1.cambiarCodigoMunicion(codigoMunicion, nuevoCodigoMunicion);
			break;
		case "2":
			codigoMunicion = comprobarCodigoMunicion(polvorin1);

			System.out.println("Introduce el nuevo tipo de municion.");
			String nuevoTipoMunicion = input.nextLine();
			polvorin1.cambiarTipoMunicion(codigoMunicion, nuevoTipoMunicion);
			break;
		case "3":
			codigoMunicion = comprobarCodigoMunicion(polvorin1);

			System.out.println("Introduce el nuevo calibre de la municion.");
			String nuevocalibre = input.nextLine();
			polvorin1.cambiarCalibre(codigoMunicion, nuevocalibre);
			break;
		case "4":
			codigoMunicion = comprobarCodigoMunicion(polvorin1);

			System.out.println("Introduce el nuevo fabricante.");
			String nuevoFabricante = input.nextLine();
			polvorin1.cambiarFabricante(codigoMunicion, nuevoFabricante);
			break;

		case "5":
			codigoMunicion = comprobarCodigoMunicion(polvorin1);

			System.out.println("Introduce el nuevo alcance efectivo.");

			alcanceEfectivo = comprobarDecimal();
			polvorin1.cambiarAlcanceEfectivo(codigoMunicion, alcanceEfectivo);
			break;
		default:
			System.out.println("Introduce una opcion valida.");
		}
		return polvorin1;
	}

	/**
	 * Metodo recibe parametro objeto, menu realiza distintas acciones, muestra
	 * objetos por distintos parametros
	 * 
	 * @param polvorin1
	 */
	public static void listarPorParametros(Polvorin polvorin1) {
		System.out.println("Elige el parametro por el que quieras listar:");
		System.out.println("1. Por tipo de municion.");
		System.out.println("2. Por calibre.");
		System.out.println("3. Por fabricante.");
		System.out.println("4. Por su alcance efectivo.");
		System.out.println("5. La o las municiones con mayor alcance efectivo.");
		menu = input.nextLine();

		switch (menu) {
		case "1":
			System.out.println("Introduce el tipo de municion por el que quieras listar.");
			tipoMunicion = input.nextLine();
			if (polvorin1.existeTipoMunicion(tipoMunicion)) {
				polvorin1.listarPorTipoMunicion(tipoMunicion);
			} else {
				System.out.println("No existe ese tipo de municion.");
			}
			break;
		case "2":
			System.out.println("Introduce el calibre por el que quieras listar.");
			calibre = input.nextLine();
			if (polvorin1.existeCalibre(calibre)) {
				polvorin1.listarPorCalibre(calibre);
			} else {
				System.out.println("No existe ese calibre.");
			}
			break;
		case "3":
			System.out.println("Introduce el fabricante por el que quieras listar.");
			fabricante = input.nextLine();
			if (polvorin1.existeFabricante(fabricante)) {
				polvorin1.listarPorFabricante(fabricante);
			} else {
				System.out.println("No existe el fabricante.");
			}
			break;
		default:
			System.out.println("Introduce una opcion valida.");
			break;
		case "4":
			System.out.println("Introduce el alcance efectivo por el que quieras listar.");
			alcanceEfectivo = comprobarDecimal();
			if (polvorin1.existeAlcanceEfectivo(alcanceEfectivo)) {
				polvorin1.listarPorAlcanceEfectivo(alcanceEfectivo);
			} else {
				System.out.println("No existe registros de ese parametro.");
			}
			break;
		case "5":
			polvorin1.municionMayorAlcanceEfectivo();
			break;
		}
	}

	/**
	 * Metodo bucle para cambiar valor de variable booleana 'continuar'
	 */
	public static void seguirFuncionando() {
		for (int i = 0; i < 1; i++) {
			System.out.println("�Deseas reealizar alguna otra opcion?");
			System.out.println("S para continuar, N para salir.");
			String respuesta = input.nextLine();
			if (respuesta.equalsIgnoreCase("S")) {
				continuar = true;
			} else if (respuesta.equalsIgnoreCase("N")) {
				continuar = false;
			} else {
				System.out.println("Respuesta no valida, introduce un valor permitido.");
				i = -1;
			}
		}
	}

	/**
	 * Metodo recibe parametro objeto, menu realiza varias funciones
	 * 
	 * @param polvorin1
	 */
	public static void menuPrincipal(Polvorin polvorin1) {
		do {
			System.out.println("****************************************************");
			System.out.println("*Elige la opcion que desees ejecutar.              *");
			System.out.println("*1. Dar de alta una nueva municion.                *");
			System.out.println("*2. Buscar una municion por su codigo.             *");
			System.out.println("*3. Eliminar una municion por su codigo.           *");
			System.out.println("*4. Listar todas las municiones.                   *");
			System.out.println("*5. Listar municiones por parametros.              *");
			System.out.println("*6. Cambiar algun dato de una municion en concreto.*");
			System.out.println("****************************************************");
			menu = input.nextLine();

			switch (menu) {
			case "1":
				System.out.println("Introduce los siguientes parametros:");
				do {
					System.out.println("Codigo de la municion.");
					codigoMunicion = input.nextLine();
					if (polvorin1.buscarMunicion(codigoMunicion) == null) {
						continuar = false;
					} else {
						System.out.println("El codigo ya existe, introduce uno v�lido");
						continuar = true;
					}
				} while (continuar);

				System.out.println("Tipo de municion.");
				tipoMunicion = input.nextLine();
				System.out.println("Calibre de la municion.");
				calibre = input.nextLine();
				System.out.println("Fabricante de la municion.");
				fabricante = input.nextLine();

				do {
					continuar = false;
					System.out.println("Introduce el alcance efectivo.");
					alcanceF = input.nextLine();
					for (int i = 0; i < alcanceF.length(); i++) {
						if ((alcanceF.charAt(i) >= '0' && alcanceF.charAt(i) <= '9') || alcanceF.charAt(i) == ',') {
							if (alcanceF.charAt(i) == ',') {
								alcanceF = alcanceF.replace(',', '.');
							}
						} else {
							System.out.println("Introduce un valor valido. Numeros decimales positivos.");
							continuar = true;
							break;
						}
					}
				} while (continuar);

				alcanceEfectivo = Double.parseDouble(alcanceF);
				polvorin1.altaMunicion(codigoMunicion, tipoMunicion, calibre, fabricante, alcanceEfectivo);
				break;
			case "2":
				System.out.println("Introduce el codigo de la municion que deseas buscar.");
				codigoMunicion = input.nextLine();
				// System.out.println(polvorin1.buscarMunicion(codigoMunicion));
				if (polvorin1.buscarMunicion(codigoMunicion) == null) {
					System.out.println("No existen registros con ese codigo.");
				} else {
					System.out.println(polvorin1.buscarMunicion(codigoMunicion));
				}
				break;
			case "3":
				System.out.println("Introduce el codigo de la municion que deseas eliminar.");
				codigoMunicion = input.nextLine();
				if (polvorin1.buscarMunicion(codigoMunicion) != null) {
					polvorin1.eliminarMunicion(codigoMunicion);
				} else {
					System.out.println("El codigo de municion no coincide con ninguna registradal.");
				}
				break;
			case "4":
				System.out.println("Todas las municiones registradas son las siguientes.");
				polvorin1.listarMuniciones();
				break;
			case "5":
				listarPorParametros(polvorin1);
				break;
			case "6":
				polvorin1 = menuCambiar(polvorin1);
			default:
				System.out.println("Introduce una opcion valida.");
			}

			seguirFuncionando();

		} while (continuar);
	}

	/**
	 * Metodo comprueba que valor introducido sea decimal, devuelve ese valor
	 * decimal transformado si es necesario
	 * 
	 * @return alcanceEfectivo
	 */
	public static double comprobarDecimal() {
		do {
			continuar = false;
			alcanceF = input.nextLine();
			for (int i = 0; i < alcanceF.length(); i++) {
				if ((alcanceF.charAt(i) >= '0' && alcanceF.charAt(i) <= '9') || alcanceF.charAt(i) == ',') {
					if (alcanceF.charAt(i) == ',') {
						alcanceF = alcanceF.replace(',', '.');
					}
				} else {
					System.out.println("Introduce un valor valido. Numeros decimales positivos.");
					continuar = true;
					break;
				}
			}
		} while (continuar);

		alcanceEfectivo = Double.parseDouble(alcanceF);
		return alcanceEfectivo;
	}

}
