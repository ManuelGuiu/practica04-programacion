package clases;
/**
 * 
 * @author ManuelGuiuRodriguez
 *
 */
public class Polvorin {
	/**
	 * @param Municion[] municiones, vector de objetos Municion
	 */
	private Municion[] municiones;

	/**
	 * Metodo da tama�o al vector municiones
	 * 
	 * @param int maximoMuniciones, tama�o del vector
	 */
	public Polvorin(int maximoMuniciones) {
		this.municiones = new Municion[maximoMuniciones];
	}

	/**
	 * Metodo crea un objeto municion introduciendo todos los parametros
	 * 
	 * @param String codigoMunicion
	 * 
	 * @param String tipoMunicion
	 * 
	 * @param String calibre
	 * 
	 * @param String fabricante
	 * 
	 * @param double alcanceEfectivo
	 */
	public void altaMunicion(String codigoMunicion, String tipoMunicion, String calibre, String fabricante,
			double alcanceEfectivo) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] == null) {
				municiones[i] = new Municion(codigoMunicion);
				municiones[i].setTipoMunicion(tipoMunicion);
				municiones[i].setCalibre(calibre);
				municiones[i].setFabricante(fabricante);
				municiones[i].setAlcanceEfectivo(alcanceEfectivo);
				break;
			}
		}
	}

	/**
	 * Metodo busca municion por codigo, si existe devuelve el objeto que coincida,
	 * si no devuelve null
	 * 
	 * @param String codigoMunicion
	 * 
	 * @return
	 *         <ul>
	 *         <li>true: el objeto</li>
	 *         <li>false: null</li>
	 *         </ul>
	 */
	public Municion buscarMunicion(String codigoMunicion) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getCodigoMunicion().equals(codigoMunicion)) {
					return municiones[i];
				}
			}
		}
		return null;
	}

	/**
	 * Metodo elimina objeto identificado por parametro
	 * 
	 * @param codigoMunicion
	 */
	public void eliminarMunicion(String codigoMunicion) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getCodigoMunicion().equals(codigoMunicion)) {
					municiones[i] = null;
				}
			}
		}
	}

	/**
	 * Metodo lista todos los objetos
	 */
	public void listarMuniciones() {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				System.out.println(municiones[i]);
			}
		}
	}

	/**
	 * Comprueba que exista el parametro
	 * 
	 * @param tipoMunicion
	 * @return
	 *         <ul>
	 *         <li>true: boolean true</li>
	 *         <li>false: boolean false</li>
	 *         </ul>
	 */
	public boolean existeTipoMunicion(String tipoMunicion) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getTipoMunicion().equals(tipoMunicion)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Muestra objetos buscados por el parametro
	 * 
	 * @param tipoMunicion
	 */
	public void listarPorTipoMunicion(String tipoMunicion) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getTipoMunicion().equals(tipoMunicion)) {
					System.out.println(municiones[i]);
				}
			}
		}
	}
	
	/**
	 * Comprueba que exista el parametro
	 * 
	 * @param calibre
	 * @return
	 *         <ul>
	 *         <li>true: boolean true</li>
	 *         <li>false: boolean false</li>
	 *         </ul>
	 */
	public boolean existeCalibre(String calibre) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getCalibre().equals(calibre)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Muestra objetos buscados por el parametro
	 * @param calibre
	 */
	public void listarPorCalibre(String calibre) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getCalibre().equals(calibre)) {
					System.out.println(municiones[i]);
				}
			}
		}
	}

	/**
	 * Comprueba que exista el parametro
	 * 
	 * @param fabricante
	 * @return
	 *         <ul>
	 *         <li>true: boolean true</li>
	 *         <li>false: boolean false</li>
	 *         </ul>
	 */
	public boolean existeFabricante(String fabricante) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getFabricante().equals(fabricante)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Muestra objetos buscados por el parametro
	 * @param fabricante
	 */
	public void listarPorFabricante(String fabricante) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getFabricante().equals(fabricante)) {
					System.out.println(municiones[i]);
				}
			}
		}
	}

	/**
	 * Comprueba que exista el parametro
	 * 
	 * @param alcanceEfectivo
	 * @return
	 *         <ul>
	 *         <li>true: boolean true</li>
	 *         <li>false: boolean false</li>
	 *         </ul>
	 */
	public boolean existeAlcanceEfectivo(double alcanceEfectivo) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getAlcanceEfectivo() == alcanceEfectivo) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Muestra objetos buscados por el parametro
	 * @param alcanceEfectivo
	 */
	public void listarPorAlcanceEfectivo(double alcanceEfectivo) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getAlcanceEfectivo() == alcanceEfectivo) {
					System.out.println(municiones[i]);
				}
			}
		}
	}

	/**
	 * Metodo muestra los objetos de mayor alcance efectivo
	 */
	public void municionMayorAlcanceEfectivo() {
		double mayorAlcance = 0;
		int registro[] = new int[municiones.length];
		int posicion = 0;
		for (int i = 0; i < registro.length; i++) {
			registro[i] = -1;
		}
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getAlcanceEfectivo() > mayorAlcance) {
					registro[posicion] = i;
					mayorAlcance = municiones[i].getAlcanceEfectivo();
					posicion++;
				}
			}
		}
		for (int i = 0; i < municiones.length; i++) {
			if (registro[i] != -1) {
				System.out.println(municiones[registro[i]]);
			}
		}
	}

	/**
	 * Metodo cambia el valor del antiguo codigo por el nuevo
	 * @param viejoCodigoMunicion
	 * @param nuevoCodigoMunicion
	 */
	public void cambiarCodigoMunicion(String viejoCodigoMunicion, String nuevoCodigoMunicion) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getCodigoMunicion().equals(viejoCodigoMunicion)) {
					municiones[i].setCodigoMunicion(nuevoCodigoMunicion);
					;
				}
			}
		}
	}

	/**
	 * Metodo cambia el tipo de municion identificado por codigoMunicion
	 * @param codigoMunicion
	 * @param nuevoTipomunicion
	 */
	public void cambiarTipoMunicion(String codigoMunicion, String nuevoTipomunicion) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getCodigoMunicion().equals(codigoMunicion)) {
					municiones[i].setTipoMunicion(nuevoTipomunicion);
					;
				}
			}
		}
	}

	/**
	 * Metodo cambia el calibre identificado por codigoMunicion
	 * @param codigoMunicion
	 * @param nuevoCalibre
	 */
	public void cambiarCalibre(String codigoMunicion, String nuevoCalibre) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getCodigoMunicion().equals(codigoMunicion)) {
					municiones[i].setCalibre(nuevoCalibre);
					;
				}
			}
		}
	}

	/**
	 * Metodo cambia el fabricante identificado por codigoMunicion
	 * @param codigoMunicion
	 * @param nuevoFabricante
	 */
	public void cambiarFabricante(String codigoMunicion, String nuevoFabricante) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getCodigoMunicion().equals(codigoMunicion)) {
					municiones[i].setFabricante(nuevoFabricante);
					;
				}
			}
		}
	}

	/**
	 * Metodo cambia el alcance efectivo identificado por codigoMunicion
	 * @param codigoMunicion
	 * @param nuevoAlcanceEfectivo
	 */
	public void cambiarAlcanceEfectivo(String codigoMunicion, double nuevoAlcanceEfectivo) {
		for (int i = 0; i < municiones.length; i++) {
			if (municiones[i] != null) {
				if (municiones[i].getCodigoMunicion().equals(codigoMunicion)) {
					municiones[i].setAlcanceEfectivo(nuevoAlcanceEfectivo);
				}
			}
		}
	}

}